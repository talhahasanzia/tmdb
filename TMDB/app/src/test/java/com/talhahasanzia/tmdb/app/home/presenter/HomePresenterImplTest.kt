package com.talhahasanzia.tmdb.app.home.presenter

import android.widget.DatePicker
import com.talhahasanzia.tmdb.app.home.contracts.HomeInteractor
import com.talhahasanzia.tmdb.app.home.contracts.HomeView
import com.talhahasanzia.tmdb.app.home.entities.Movie
import com.talhahasanzia.tmdb.app.home.entities.MovieListDto
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * Author: Talha Hasan Zia
 *
 *
 * Created: 7:48 PM - 06/05/2019
 * ----------------------------------------------
 *
 *
 * Description:
 * Presenter unit test cases
 */
@RunWith(MockitoJUnitRunner::class)
class HomePresenterImplTest {

    @Mock
    lateinit var homeView: HomeView

    @Mock
    lateinit var homeInteractor: HomeInteractor

    @Mock
    lateinit var datePicker: DatePicker

    lateinit var presenterImpl: HomePresenterImpl

    @Before
    fun setUp() {
        presenterImpl = HomePresenterImpl(homeView, homeInteractor)
    }


    @Test
    fun initMovies() {
        presenterImpl.initMovies()
        Mockito.verify(homeInteractor).fetchMovies(1)
    }

    @Test
    fun onNotFound() {
        presenterImpl.onNotFound()
        Mockito.verify(homeView).showMovieError()
    }

    @Test
    fun onMoviesFound() {
        presenterImpl.onMoviesFound(getMovieList().results, getMovieList().total_results)
        Mockito.verify(homeView).setMovies(getMovieList().results, getMovieList().total_results)
    }

    @Test
    fun onFailure() {
        val message = "Error message"
        presenterImpl.onFailure(message)
        Mockito.verify(homeView).showToast(message)
    }

    @Test
    fun onDateSet() {
        presenterImpl.onDateSet(datePicker, 2019, 12, 1)
        Mockito.verify(homeInteractor).fetchMoviesByDate(getDateString(2019, 12, 1))
    }

    @Test
    fun onLoadMore() {
        presenterImpl.onLoadMore(2)
        Mockito.verify(homeInteractor).fetchMovies(2)
    }

    @Test
    fun onMoreFound() {
        presenterImpl.onMoreFound(getMovieList().results)
        Mockito.verify(homeView).addMovies(getMovieList().results)
    }

    // create dummy data for test
    private fun getMovieList(): MovieListDto {
        val movieList = ArrayList<Movie>()
        movieList.add(Movie(1, 1, false, 22.0, "Sample Title",
                5.5, "", "", "", ArrayList(),
                "", false, "", ""))
        val movieListDto = MovieListDto(1, 22, 455, movieList)
        return movieListDto
    }

    // prepare dummy date string
    private fun getDateString(year: Int, month: Int, day: Int): String {
        return year.toString().plus("-").plus(month.toString()).plus("-").plus(day)

    }

}