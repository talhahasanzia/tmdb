package com.talhahasanzia.tmdb.app.home.interactor

import com.talhahasanzia.tmdb.app.home.api.MovieListRequest
import com.talhahasanzia.tmdb.app.home.contracts.HomeInteractorOut
import com.talhahasanzia.tmdb.app.home.entities.Movie
import com.talhahasanzia.tmdb.app.home.entities.MovieListDto
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


/**
 * Author: Talha Hasan Zia
 *
 *
 * Created: 6:54 PM - 06/05/2019
 * ----------------------------------------------
 *
 *
 * Description:
 * Tests for interactor
 */
@RunWith(MockitoJUnitRunner::class)
class HomeInteractorImplTest {

    @Mock
    lateinit var movieLisRequest: MovieListRequest

    @Mock
    lateinit var homeInteractorOut: HomeInteractorOut

    lateinit var homeInteractorImpl: HomeInteractorImpl


    @Before
    fun setUp() {
        homeInteractorImpl = HomeInteractorImpl(movieLisRequest)
        homeInteractorImpl.out = homeInteractorOut

        // since its a unit test, do not make actual call, just test if the execute() method was invoked
        Mockito.doNothing().`when`(movieLisRequest).execute(homeInteractorImpl)
    }


    @Test
    fun fetchMovies() {
        homeInteractorImpl.fetchMovies(1)
        Mockito.verify(movieLisRequest).execute(homeInteractorImpl)
    }

    @Test
    fun fetchMoviesByDate() {
        homeInteractorImpl.fetchMoviesByDate("2019-12-1")
        Mockito.verify(movieLisRequest).execute(homeInteractorImpl)
    }

    @Test
    fun onSuccess() {
        val movieListDto = getMovieList()
        homeInteractorImpl.onSuccess(movieListDto)
        Mockito.verify(homeInteractorOut).onMoviesFound(movieListDto.results, movieListDto.total_results)
    }


    @Test
    fun onEmptySuccess() {
        val movieListDto = getMovieEmptyList()
        homeInteractorImpl.onSuccess(movieListDto)
        Mockito.verify(homeInteractorOut).onNotFound()
    }


    @Test
    fun onLoadMoreSuccess() {
        movieLisRequest.page = 2
        val movieListDto = getMovieList()
        homeInteractorImpl.onSuccess(movieListDto)
        Mockito.verify(homeInteractorOut).onMoreFound(movieListDto.results)
    }


    @Test
    fun onFailure() {
        val message = "Dummy error"
        homeInteractorImpl.onFailure(message, -1)
        Mockito.verify(homeInteractorOut).onFailure(message)
    }

    // create dummy data for test
    private fun getMovieList(): MovieListDto {
        val movieList = ArrayList<Movie>()
        movieList.add(Movie(1, 1, false, 22.0, "Sample Title",
                5.5, "", "", "", ArrayList(),
                "", false, "", ""))
        val movieListDto = MovieListDto(1, 22, 455, movieList)
        return movieListDto
    }

    // create dummy data with empty list for test
    private fun getMovieEmptyList(): MovieListDto {
        val movieList = ArrayList<Movie>()
        val movieListDto = MovieListDto(1, 22, 455, movieList)
        return movieListDto
    }
}