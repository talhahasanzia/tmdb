package com.talhahasanzia.tmdb.app.home.presenter

import android.widget.DatePicker
import com.talhahasanzia.tmdb.app.home.contracts.HomeInteractor
import com.talhahasanzia.tmdb.app.home.contracts.HomeInteractorOut
import com.talhahasanzia.tmdb.app.home.contracts.HomePresenter
import com.talhahasanzia.tmdb.app.home.contracts.HomeView
import com.talhahasanzia.tmdb.app.home.entities.Movie
import javax.inject.Inject

/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:14 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 *
 */
class HomePresenterImpl @Inject constructor(var homeView: HomeView, var homeInteractor: HomeInteractor) : HomePresenter, HomeInteractorOut {


    //----------- Presenter Contract Methods---------//

    override fun initMovies() {
        // initial call with page number 1
        homeInteractor.fetchMovies(1)
    }


    override fun onNotFound() {
        homeView.showMovieError()
    }

    override fun onMoviesFound(results: List<Movie>, totalResults: Int) {
        homeView.setMovies(results, totalResults)
    }


    override fun onFailure(message: String?) {
        homeView.showToast(message!!)
    }

    override fun onLoadMore(page: Int) {
        homeInteractor.fetchMovies(page)
    }

    override fun onMoreFound(results: List<Movie>) {
        homeView.addMovies(results)
    }


    //----------- DateTime Picker Methods---------//
    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
        val date: String = year.toString().plus("-").plus(month.toString()).plus("-").plus(day)
        homeInteractor.fetchMoviesByDate(date)
    }


}