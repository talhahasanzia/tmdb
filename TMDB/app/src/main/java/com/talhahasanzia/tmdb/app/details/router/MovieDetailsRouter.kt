package com.talhahasanzia.tmdb.app.details.router

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.talhahasanzia.tmdb.app.details.view.MovieDetailsActivity
import com.talhahasanzia.tmdb.commons.contracts.BaseRouter

/**
 * Author: Talha Hasan Zia
 *
 * Created: 7:41 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 *
 */
class MovieDetailsRouter : BaseRouter {

    companion object {
        const val MOVIE_EXTRAS = "movie_extras"
    }

    override fun route(context: Context, bundle: Bundle) {
        val intent = Intent(context, MovieDetailsActivity::class.java)
        intent.putExtra(MOVIE_EXTRAS, bundle.getSerializable(MovieDetailsRouter.MOVIE_EXTRAS))
        context.startActivity(intent)
    }

}