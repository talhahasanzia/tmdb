package com.talhahasanzia.tmdb.app.details.view

import android.os.Bundle
import com.squareup.picasso.Picasso
import com.talhahasanzia.tmdb.R
import com.talhahasanzia.tmdb.app.details.contracts.MovieDetailsView
import com.talhahasanzia.tmdb.app.details.dependencies.DaggerMovieDetailsComponent
import com.talhahasanzia.tmdb.app.details.dependencies.MovieDetailsModule
import com.talhahasanzia.tmdb.app.details.router.MovieDetailsRouter
import com.talhahasanzia.tmdb.app.home.entities.Movie
import com.talhahasanzia.tmdb.commons.ui.BaseActivity
import com.talhahasanzia.tmdb.commons.utils.Constants
import kotlinx.android.synthetic.main.activity_movie_details.*
import javax.inject.Inject

class MovieDetailsActivity : BaseActivity(), MovieDetailsView {

    //------------ Fields -----------//
    @Inject
    lateinit var picasso: Picasso

    //------------ BaseActivity Methods -----------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        setTitleBackEnabled(getMovieFromExtras().title)
        init()
    }


    //------------ View Methods -----------//
    override fun inject() {
        DaggerMovieDetailsComponent.builder()
                .movieDetailsModule(MovieDetailsModule(this))
                .build()
                .inject(this)
    }

    //------------ Private Methods -----------//
    private fun init() {
        val movie = getMovieFromExtras()
        tvTitle.text = movie.title
        tvReleaseDate.text = getString(R.string.release_Date_prefix).plus(movie.release_date)
        tvOverView.text = movie.overview
        picasso.load(Constants.BASE_IMAGE_URL.plus(movie.poster_path)).placeholder(R.drawable.ic_hourglass).into(ivPoster)
    }

    private fun getMovieFromExtras(): Movie {
        return intent.getSerializableExtra(MovieDetailsRouter.MOVIE_EXTRAS) as Movie
    }


}
