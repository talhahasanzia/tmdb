package com.talhahasanzia.tmdb.app.home.entities

import com.talhahasanzia.tmdb.commons.network.BaseResponseDto

/**
 * Author: Talha Hasan Zia
 *
 * Created: 3:40 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 *
 */
data class MovieListDto(
        val page: Int,
        val total_results: Int,
        val total_pages: Int,
        val results: List<Movie>
) : BaseResponseDto {

    override var success: Boolean = false


}