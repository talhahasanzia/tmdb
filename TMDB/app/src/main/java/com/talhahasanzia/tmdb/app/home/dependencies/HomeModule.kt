package com.talhahasanzia.tmdb.app.home.dependencies

import com.talhahasanzia.tmdb.app.home.adapters.MoviesAdapter
import com.talhahasanzia.tmdb.app.home.api.MovieListRequest
import com.talhahasanzia.tmdb.app.home.contracts.HomeInteractor
import com.talhahasanzia.tmdb.app.home.contracts.HomePresenter
import com.talhahasanzia.tmdb.app.home.interactor.HomeInteractorImpl
import com.talhahasanzia.tmdb.app.home.presenter.HomePresenterImpl
import com.talhahasanzia.tmdb.app.home.view.HomeActivity
import dagger.Module
import dagger.Provides

/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:09 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Module that provide all the dependencies that Home feature needed
 *
 * Provide HomeView dependency in constructor
 */
@Module
class HomeModule(var homeActivity: HomeActivity) {


    @Provides
    fun providePresenter(homeInteractor: HomeInteractor): HomePresenter {
        val homePresenterImpl = HomePresenterImpl(homeActivity, homeInteractor)
        // since it is a cyclic dependency Dagger wont resolve it
        // so we initialize it ourselves here
        homeInteractor.out = homePresenterImpl
        return homePresenterImpl
    }

    @Provides
    fun provideHomeInteractor(): HomeInteractor = HomeInteractorImpl(MovieListRequest())

    @Provides
    fun provideAdapter(): MoviesAdapter = MoviesAdapter(homeActivity, homeActivity)


}