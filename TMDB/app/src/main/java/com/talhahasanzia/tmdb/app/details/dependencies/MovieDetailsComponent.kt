package com.talhahasanzia.tmdb.app.details.dependencies

import com.talhahasanzia.tmdb.app.details.view.MovieDetailsActivity
import dagger.Component

/**
 * Author: Talha Hasan Zia
 *
 * Created: 11:31 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Component for Movie Details Feature
 */
@Component(modules = [MovieDetailsModule::class])
interface MovieDetailsComponent {
    fun inject(movieDetailsActivity: MovieDetailsActivity)
}