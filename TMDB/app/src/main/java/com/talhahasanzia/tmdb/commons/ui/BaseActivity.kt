package com.talhahasanzia.tmdb.commons.ui

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.talhahasanzia.tmdb.R
import com.talhahasanzia.tmdb.commons.contracts.BaseView

/**
 * Author: Talha Hasan Zia
 * Created: 6:35 PM - 02/05/2019
 *
 * BaseActivity that holds common functionality
 */
abstract class BaseActivity : AppCompatActivity(), BaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // call inject of view so that all the dependencies are satisfied at the start
        inject()
    }

    /**
     * Set title
     */
    protected fun setTitle(title: String) {
        val actionBar = supportActionBar
        actionBar?.title = title
    }

    /**
     * Set title and enable back key
     */
    protected fun setTitleBackEnabled(title: String) {
        val actionBar = supportActionBar
        actionBar?.title = title
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)

    }


    /**
     * Show Snackbar with message
     */
    override fun showSnackBar(message: String) {

        val connectivitySnackbar = Snackbar.make(findViewById<View>(android.R.id.content),
                message, Snackbar.LENGTH_LONG)

        connectivitySnackbar.setActionTextColor(Color.WHITE)

        connectivitySnackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))

        connectivitySnackbar.show()

    }

    /**
     * Shows a toast with message provided
     */
    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}