package com.talhahasanzia.tmdb.app.home.view

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.talhahasanzia.tmdb.R
import com.talhahasanzia.tmdb.app.details.router.MovieDetailsRouter
import com.talhahasanzia.tmdb.app.home.adapters.EndlessRecyclerViewScrollListener
import com.talhahasanzia.tmdb.app.home.adapters.MoviesAdapter
import com.talhahasanzia.tmdb.app.home.contracts.HomePresenter
import com.talhahasanzia.tmdb.app.home.contracts.HomeView
import com.talhahasanzia.tmdb.app.home.dependencies.DaggerHomeComponent
import com.talhahasanzia.tmdb.app.home.dependencies.HomeModule
import com.talhahasanzia.tmdb.app.home.entities.Movie
import com.talhahasanzia.tmdb.commons.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*
import javax.inject.Inject


/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:01 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * HomeActivity that shows list of Movies and allow filter by release date
 *
 */
class HomeActivity : BaseActivity(), HomeView {


    //------------ Fields -----------//
    @Inject
    lateinit var presenter: HomePresenter
    @Inject
    lateinit var adapter: MoviesAdapter


    //------------ BaseActivity Methods -----------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setTitle(getString(R.string.app_name))
        initRecyclerView()
        presenter.initMovies()
    }

    // Create options menu to show filter option
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.options_menu, menu)
        return true
    }

    // handle clicks on filter options
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.filter -> showDateTimePicker()
            R.id.clearFilter -> presenter.initMovies()
        }
        return true
    }

    //------------ View Methods -----------//
    override fun inject() {
        DaggerHomeComponent.builder()
                .homeModule(HomeModule(this))
                .build()
                .inject(this)
    }

    override fun setMovies(results: List<Movie>, totalResults: Int) {
        // hide progress bar
        progressBar.visibility = View.INVISIBLE

        // show results status
        showSnackBar("Showing " + results.size.toString() + " out of " + totalResults.toString() + " movies released this year")

        // update dataset
        adapter.setResults(results)
    }


    override fun addMovies(results: List<Movie>) {
        adapter.updateResults(results)
    }


    override fun showMovieError() {
        progressBar.visibility = View.INVISIBLE
        showToast(getString(R.string.no_movies_error))
    }


    //------------ Movie Item Listener (Movies Adapter) Methods -----------//
    override fun onMovieItemClicked(result: Movie) {
        // prepare data to be sent to details screen
        val movieDetails = Bundle()
        movieDetails.putSerializable(MovieDetailsRouter.MOVIE_EXTRAS, result)
        // call router to navigate to that screen
        MovieDetailsRouter().route(this, movieDetails)
    }


    //------------ Private Methods -----------//
    private fun initRecyclerView() {

        // show progress bar
        progressBar.visibility = View.VISIBLE

        // initialize layout manager
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvMovies.layoutManager = linearLayoutManager
        rvMovies.setHasFixedSize(true)

        rvMovies.addOnScrollListener(MoviesScrollListener(linearLayoutManager))

        // set adapter
        rvMovies.adapter = adapter
    }


    private fun showDateTimePicker() {
        // prepare DateTime picker initial values
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // set initial values
        val datePickerDialog = DatePickerDialog(this, presenter, year, month, day)

        // show DateTime picker
        datePickerDialog.show()

    }


    //------------ Endless scroll listener ----------------//

    inner class MoviesScrollListener(linearLayoutManager: LinearLayoutManager) : EndlessRecyclerViewScrollListener(linearLayoutManager) {
        override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
            presenter.onLoadMore(page + 1)
        }
    }


}