package com.talhahasanzia.tmdb.commons.application

import android.app.Application

/**
 * Author: Talha Hasan Zia
 *
 * Created: 7:55 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Application class object
 */
class TMDBApp : Application() {

}