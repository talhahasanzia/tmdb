package com.talhahasanzia.tmdb.commons.contracts

import android.content.Context
import android.os.Bundle

/**
 * Author: Talha Hasan Zia
 *
 * Created: 7:39 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Base router contract to hide routing details from caller
 * Call startActivity when implementing this method to navigate to other Activity
 */
interface BaseRouter {
    fun route(context: Context, bundle: Bundle)
}