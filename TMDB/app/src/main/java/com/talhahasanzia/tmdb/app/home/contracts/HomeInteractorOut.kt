package com.talhahasanzia.tmdb.app.home.contracts

import com.talhahasanzia.tmdb.app.home.entities.Movie

/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:17 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Home Interactor out for posting results back to presenter
 */
interface HomeInteractorOut{
    fun onNotFound()
    fun onMoviesFound(results: List<Movie>, totalResults: Int)
    fun onFailure(message: String?)
    fun onMoreFound(results: List<Movie>)

}