package com.talhahasanzia.tmdb.app.home.contracts

import com.talhahasanzia.tmdb.app.home.entities.MovieListDto
import com.talhahasanzia.tmdb.commons.contracts.BaseInteractor
import com.talhahasanzia.tmdb.commons.network.ResponseCallback

/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:00 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Home Interactor for Business logics and API calls
 */
interface HomeInteractor : BaseInteractor, ResponseCallback<MovieListDto> {
    // fetch movie initially
    fun fetchMovies(page: Int)

    // fetch movies by date
    fun fetchMoviesByDate(date: String)

    // abstract property to allow initialization in module
    var out: HomeInteractorOut
}