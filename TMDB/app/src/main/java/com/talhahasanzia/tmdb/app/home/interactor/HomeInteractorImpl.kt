package com.talhahasanzia.tmdb.app.home.interactor

import com.talhahasanzia.tmdb.app.home.api.MovieListRequest
import com.talhahasanzia.tmdb.app.home.contracts.HomeInteractor
import com.talhahasanzia.tmdb.app.home.contracts.HomeInteractorOut
import com.talhahasanzia.tmdb.app.home.entities.MovieListDto
import com.talhahasanzia.tmdb.commons.utils.DateUtil
import java.util.*
import javax.inject.Inject

/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:16 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Interactor for delivering business logic for Home feature
 */
class HomeInteractorImpl @Inject constructor(var request: MovieListRequest) : HomeInteractor {


    // InteractorOut for posting results
    override lateinit var out: HomeInteractorOut

    // call for fetching movies
    override fun fetchMovies(page: Int) {
        // first time call
        request.page = page
        // set formatted end date for release
        val calendar = Calendar.getInstance()
        // create request
        request.releaseDateEnd = DateUtil.getFormattedDate(calendar.timeInMillis)
        // set formatted start date (start of the year for latest movies)
        calendar.set(Calendar.DAY_OF_YEAR, 1)
        request.releaseDateStart = DateUtil.getFormattedDate(calendar.timeInMillis)
        // make request
        request.execute(this)
    }

    // fetch movies by date selected by user
    override fun fetchMoviesByDate(date: String) {
        request.releaseDateStart = date
        request.releaseDateEnd = date
        request.execute(this)
    }


    // on successful fetch
    override fun onSuccess(response: MovieListDto) {
        // it was success but no items were present
        if (response.results.isEmpty()) {
            out.onNotFound()
        } else if (request.page > 1){
            // call was made to load more
            out.onMoreFound(response.results)
        }
        else {
            out.onMoviesFound(response.results, response.total_results)
        }
    }

    //on failure fetching movies
    override fun onFailure(message: String?, code: Int) {
        out.onFailure(message)
    }


}