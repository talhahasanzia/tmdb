package com.talhahasanzia.tmdb.app.home.api


import com.talhahasanzia.tmdb.app.home.entities.MovieListDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:03 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * End-point for movie list
 */
interface MovieListAPI {

    @GET("discover/movie")
    fun getMovies(@Query(MovieListRequest.LANGUAGE) language: String,
                  @Query(MovieListRequest.SORT_BY) sortBy: String,
                  @Query(MovieListRequest.RELEASE_DATE_START) releaseStart: String,
                  @Query(MovieListRequest.RELEASE_DATE_END) releaseEnd: String,
                  @Query(MovieListRequest.INCLUDE_ADULT) includeAdult: Boolean,
                  @Query(MovieListRequest.INCLUDE_VIDEO) includeVideo: Boolean,
                  @Query(MovieListRequest.PAGE) page: Int
    ): Call<MovieListDto>
}