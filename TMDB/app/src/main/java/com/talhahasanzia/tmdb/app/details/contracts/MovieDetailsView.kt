package com.talhahasanzia.tmdb.app.details.contracts

import com.talhahasanzia.tmdb.commons.contracts.BaseView

/**
 * Author: Talha Hasan Zia
 *
 * Created: 10:11 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * UI Layer for Movies Details Feature
 */
interface MovieDetailsView :BaseView {
}