package com.talhahasanzia.tmdb.app.home.contracts

import com.talhahasanzia.tmdb.app.home.adapters.MoviesAdapter
import com.talhahasanzia.tmdb.app.home.entities.Movie
import com.talhahasanzia.tmdb.commons.contracts.BaseView

/**
 * Author: Talha Hasan Zia
 *
 * Created: 7:57 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * UI layer for movie list to be displayed
 */
interface HomeView : BaseView, MoviesAdapter.MovieItemListener {
    fun setMovies(results: List<Movie>, totalResults: Int)
    fun showMovieError()
    fun addMovies(results: List<Movie>)
}