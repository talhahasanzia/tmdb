package com.talhahasanzia.tmdb.app.details.dependencies

import com.squareup.picasso.Picasso
import com.talhahasanzia.tmdb.app.details.router.MovieDetailsRouter
import com.talhahasanzia.tmdb.app.details.view.MovieDetailsActivity
import com.talhahasanzia.tmdb.app.home.entities.Movie
import dagger.Module
import dagger.Provides

/**
 * Author: Talha Hasan Zia
 *
 * Created: 11:30 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Dependencies for Movie Details Feature are provided here
 */
@Module
class MovieDetailsModule(var movieDetailsActivity: MovieDetailsActivity) {


    @Provides
    fun providePicasso(): Picasso = Picasso.get()


}