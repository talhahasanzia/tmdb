package com.talhahasanzia.tmdb.commons.contracts

/**
 * Author: Talha Hasan Zia
 * Created: 6:35 PM - 02/05/2019
 *
 *
 * View (VIPER) that will hold common signatures for all Views
 * This will allow certain methods to be called from Presenter without requiring instance of activity
 * This holds contract that every View should implement
 */
interface BaseView {
    /**
     * Inject all the dependencies this class needs
     */
    fun inject()

    /**
     * Show snackbar by simply calling this method
     */
    fun showSnackBar(message: String)

    /**
     * Simplified show toast method
     */
    fun showToast(message: String)
}