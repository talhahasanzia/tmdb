package com.talhahasanzia.tmdb.app.home.api

import com.talhahasanzia.tmdb.app.home.entities.MovieListDto
import com.talhahasanzia.tmdb.commons.network.BaseRequest
import retrofit2.Call
import retrofit2.Retrofit

/**
 * Author: Talha Hasan Zia
 *
 * Created: 3:40 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Request specification for movie list end-point.
 * This will be used by BaseRequest when the caller of MovieListRequest calls execute().
 */
open class MovieListRequest : BaseRequest<MovieListDto>() {

    // page number of movies
    var page: Int = 1
    var releaseDateStart = ""
    var releaseDateEnd = ""
    val LANGUAGE_EN_US = "en-US"
    val SORT_RELEASE_DATE_DESC = "release_date.desc"


    /**
     * Only request class knows details about the request
     * Here we make constants for keys that will be used in this request's Query parameter
     */
    companion object {
        const val LANGUAGE: String = "language"
        const val SORT_BY: String = "sort_by"
        const val INCLUDE_ADULT: String = "include_adult"
        const val INCLUDE_VIDEO: String = "include_video"
        const val RELEASE_DATE_END: String = "primary_release_date.lte"
        const val RELEASE_DATE_START: String = "primary_release_date.gte"
        const val PAGE: String = "page"

    }

    /**
     * Make the Call<> object that will be invoked when execute is called of this class
     */
    override fun make(retrofit: Retrofit): Call<MovieListDto> {
        return retrofit.create(MovieListAPI::class.java).getMovies(
                LANGUAGE_EN_US,
                SORT_RELEASE_DATE_DESC,
                releaseDateStart,
                releaseDateEnd,
                false,
                true,
                page

        )
    }
}