package com.talhahasanzia.tmdb.commons.network

import retrofit2.Call
import retrofit2.Retrofit

/**
 * Author: Talha Hasan Zia
 *
 * Created: 11:28 AM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * This is the interface that will define contract for every Request Logic in the app.
 * This is not Retrofit Call T, this is an abstraction over it so app does not have to deal with retrofit details directly,
 * network layer has this responsibility.
 *
 *
 * To define request specifications like end points or payloads extend BaseRequest and implement this interface to specify Response type T.
 * This layer will act as mediator between pure network logic and application layer
 * Any changes in Retrofit or Network layer should not effect app layer directly
 */
interface Request<T : BaseResponseDto> {
    fun make(retrofit: Retrofit): Call<T>
    fun execute(responseCallback: ResponseCallback<T>)
}
