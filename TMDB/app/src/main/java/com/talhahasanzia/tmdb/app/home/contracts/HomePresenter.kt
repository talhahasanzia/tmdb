package com.talhahasanzia.tmdb.app.home.contracts

import android.app.DatePickerDialog
import com.talhahasanzia.tmdb.commons.contracts.BasePresenter

/**
 * Author: Talha Hasan Zia
 *
 * Created: 7:59 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Home Presenter to handle validation logic and communicate view and interactor layers
 */
interface HomePresenter : BasePresenter, DatePickerDialog.OnDateSetListener {
    fun initMovies()
    fun onLoadMore(page : Int)
}