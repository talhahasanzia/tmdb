package com.talhahasanzia.tmdb.commons.utils

/**
 * Author: Talha Hasan Zia
 *
 * Created: 11:30 AM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * App-wide constants goes here
 */
object Constants {
    val BASE_URL = "https://api.themoviedb.org/3/"
    val REQUEST_TIMEOUT: Long = 10
    val API_KEY = "441f37d4d70b37a51aca2e31e767b4c5"
    val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w500"
}