package com.talhahasanzia.tmdb.commons.contracts

/**
 * Author: Talha Hasan Zia
 *
 * Created: 7:00 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Presenter that will hold common signatures for all Presenters
 */
interface BasePresenter {
}