package com.talhahasanzia.tmdb.app.home.dependencies

import com.talhahasanzia.tmdb.app.home.view.HomeActivity
import dagger.Component

/**
 * Author: Talha Hasan Zia
 *
 * Created: 8:32 PM - 02/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Component for Home feature
 */
@Component(modules = [HomeModule::class])
interface HomeComponent {
    fun inject(homeActivity: HomeActivity)
}