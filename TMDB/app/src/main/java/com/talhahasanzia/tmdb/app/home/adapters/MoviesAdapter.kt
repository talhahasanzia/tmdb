package com.talhahasanzia.tmdb.app.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.talhahasanzia.tmdb.R
import com.talhahasanzia.tmdb.app.home.entities.Movie
import com.talhahasanzia.tmdb.commons.utils.Constants
import kotlinx.android.synthetic.main.movie_list_item.view.*

/**
 * Author: Talha Hasan Zia
 *
 * Created: 7:01 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 *
 * Adapter that is responsible for displaying movies on the home screen
 *
 */
class MoviesAdapter(var context: Context, var movieItemListener: MovieItemListener) : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    private var results = ArrayList<Movie>()

    // replace data as new
    fun setResults(results: List<Movie>) {
        this.results = results as ArrayList<Movie>
        notifyDataSetChanged()
    }

    // append data to existing one
    fun updateResults(results: List<Movie>) {
        this.results.addAll(results)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MoviesViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.movie_list_item, viewGroup, false)
        return MoviesViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: MoviesViewHolder, position: Int) {
        viewHolder.bind(results.get(position))
    }

    override fun getItemCount(): Int {
        return results.size
    }


    inner class MoviesViewHolder(private var view: View) : RecyclerView.ViewHolder(view) {

        // bind data with views
        fun bind(result: Movie) {
            // set movie title
            view.tvMovieTitle.text = result.title
            // load movie poster
            Picasso.get().load(Constants.BASE_IMAGE_URL.plus(result.poster_path)).placeholder(R.drawable.ic_hourglass).into(view.ivPoster)

            view.setOnClickListener { _ -> movieItemListener.onMovieItemClicked(result) }
        }

    }


    /**
     * Listener that will dispatch click event to the Activity = with item data on which the user has clicked
     */
    interface MovieItemListener {
        fun onMovieItemClicked(result: Movie)
    }
}