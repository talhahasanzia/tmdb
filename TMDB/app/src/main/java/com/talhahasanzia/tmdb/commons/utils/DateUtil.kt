package com.talhahasanzia.tmdb.commons.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat

/**
 * Author: Talha Hasan Zia
 *
 * Created: 9:42 PM - 03/05/2019
 * ----------------------------------------------
 *
 * Description:
 * Date formatting helper
 */
object DateUtil {
    @SuppressLint("SimpleDateFormat")
    fun getFormattedDate(timestamp: Long): String {
        return SimpleDateFormat("yyyy-MM-dd").format(timestamp)
    }
}