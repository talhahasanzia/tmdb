# tmdb - the movies app
This is a demo app which shows list of movies and their details using TMDB API v3. The app shows movie list of latest releases, allows user to filter movie by specific dates and also see the details of movie about its overview and release date

## Overview

- Kotlin is main language used in the project
- VIPER is used as architecture
- Dagger2 is used as dependency manager
- Network layer is separated via an abstract layer to promote loosely coupled code
- Unit testing

## Kotlin
- The app's core language is Kotlin, which is preferred language for Android Apps Development

## VIPER

- Every layer in VIPER has Base contract to impose app wide rules
- All the UI interaction and display logic goes in View layer which is Activity in our case
- Presenter handles validation logic
- Interactor handles business logic and network calls

## Dagger2

- Dagger2 is used as dependency manager
- All the dependencies are injected using Dagger2
- Each feature has its own dependency module


## Network
- Retrofit is used for networking in this project
- Network layer is created to allow abstraction between the core business logic and network library
- Simple onSuccess and onFailure callbacks are implemented in Interactor, which does not know the real request or response logic


## Unit Testing
- Unit tests are written for Interactor and Presenter
- All unit tests are written in Kotlin 
- Mockito is used to mock the objects that are required in the unit test by the code



